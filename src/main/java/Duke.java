import java.util.Scanner;

public class Duke {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String line;

        Task[] taskList = new Task[100];
        int taskCount = 0;

        System.out.println("Hello! I'm Duke");
        System.out.println("What can I do for you?\n");

        while (input.hasNext()) {
            line = input.nextLine();
            String[] words = line.split(" ", 2);
            String keyword = words[0];
            Task newTask;

            switch (keyword) {
                case "done":
                    int index = Integer.parseInt(words[1]);
                    taskList[index-1].markAsDone();
                    break;
                case "bye":
                    System.out.println("Bye. Hope to see you again soon!");
                    System.exit(0);
                case "list":
                    System.out.println("Here are the tasks in your list:");
                    for (int i = 0; i < taskCount; i++) {
                        System.out.print(i + 1);
                        System.out.println("." + taskList[i]);
                    }
                    break;
                case "deadline":
                    String[] deadlineDetails = words[1].split(" /", 2);
                    newTask = new Deadline(deadlineDetails[0], deadlineDetails[1]);
                    taskList[taskCount] = newTask;
                    System.out.println("Got it. I've added this task:" );
                    System.out.println("  " + taskList[taskCount]);
                    taskCount++;
                    if (taskCount == 1) {
                        System.out.println("Now you have " + taskCount + " task in the list.");
                    } else if (taskCount > 1) {
                        System.out.println("Now you have " + taskCount + " tasks in the list.");
                    }
                    break;
                case "event":
                    String[] eventDetails = words[1].split(" /", 2);
                    newTask = new Event(eventDetails[0], eventDetails[1]);
                    taskList[taskCount] = newTask;
                    System.out.println("Got it. I've added this task:" );
                    System.out.println("  " + taskList[taskCount]);
                    taskCount++;
                    if (taskCount == 1) {
                        System.out.println("Now you have " + taskCount + " task in the list.");
                    } else if (taskCount > 1) {
                        System.out.println("Now you have " + taskCount + " tasks in the list.");
                    }
                    break;
                case "todo":
                    newTask = new ToDo(words[1]);
                    taskList[taskCount] = newTask;
                    System.out.println("Got it. I've added this task:" );
                    System.out.println("  " + taskList[taskCount]);
                    taskCount++;
                    if (taskCount == 1) {
                        System.out.println("Now you have " + taskCount + " task in the list.");
                    } else if (taskCount > 1) {
                        System.out.println("Now you have " + taskCount + " tasks in the list.");
                    }
                    break;
                default:
                    break;
            }

        }

    }
}
