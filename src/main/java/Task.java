public class Task {
	protected String description;
	protected boolean isDone;

	public Task(String description) {
		this.description = description;
		this.isDone = false;
	}

	public String getStatusIcon() {
		return (isDone ? "✓" : "✗"); //return tick or X symbols
	}

	public void markAsDone() {
		isDone = true;
		System.out.println("Nice! I've marked this task as done:");
		System.out.println("  [" + this.getStatusIcon() + "] " + description);
	}

	@Override
	public String toString() {
		return "[" + this.getStatusIcon() + "] " + description;
	}
}
